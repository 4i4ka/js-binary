const BASE_API_URL = 'https://api.github.com/';
const SECURITY_HEADERS = {
    headers: {
        authorization: "token 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"
    }
};

export function callApi(endpoint, method = 'GET') {
    const url = BASE_API_URL + endpoint;
    const options = {method, ...SECURITY_HEADERS};

    return fetch(url, options)
        .then(response =>
            response.ok
                ? response.json()
                : Promise.reject(Error('Failed to load'))
        )
        .catch(error => {
            throw error
        });
}