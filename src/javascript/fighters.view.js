import View from "./view";
import FighterView from "./fighter.view";

class FightersView extends View {
    fightersDetailsMap = new Map();

    constructor(fighters) {
        super();

        this.handleClick = this.handleFighterClick.bind(this);
        this.createFighters(fighters);
    }

    createFighters(fighters) {
        const fighterElements = fighters.map(fighter => {
            const fighterView = new FighterView(fighter, this.handleClick);

            return fighterView.element;
        });

        this.element = this.createElement({tagName: 'div', className: 'fighters'});
        this.element.append(...fighterElements);
    }

    handleFighterClick(event, fighter) {
        this.fightersDetailsMap.set(fighter._id, fighter);
        console.log('clicked')
        // get from map or load info and add to fightersMap
        // show modal with fighter info
        // allow to edit health and power in this modal
    }
}

export default FightersView;